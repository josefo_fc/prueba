SELECT * FROM datos;

ALTER TABLE datos MODIFY imagen BLOB;
    
    
create table recompensa (
id_recomp int auto_increment PRIMARY KEY,
valor varchar(50));

ALTER TABLE datos 
ADD constraint fk_id_recomp
foreign key (fk_id_recomp) REFERENCES recompensa(id_recomp);
